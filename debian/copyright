Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hyperspy
Source: https://github.com/hyperspy/hyperspy

Files: *
Copyright: 2007-2016 The HyperSpy developers
License: GPL-3

Files: hyperspy/external/mpfit/*
Copyright: 1997-2002 Craig Markwardt; 2002 Mark Rivers; 2008 Sergey Koposov; 20011 The HyperSpy Developers
License: BSD-2

Files: hyperspy/external/mpfit/mpfitexpr.py
Copyright: 2009 Sergey Koposov
License: GPL-3

Files: hyperspy/external/progressbar.py
Copyright: 2008 Nilton Volpato
License: BSD-3

Files: hyperspy/io_plugins/digital_micrograph.py hyperspy/misc/io/utils_readfile.py
Copyright: 2010 Stefano Mazzucco; 2011-2016 The HyperSpy developers
License: GPL-3

Files: hyperspy/learn/mlpca.py
Copyright: 1997 Darren T. Andrews, Peter D. Wentzell; 2007-2016 The HyperSpy developers
License: GPL-3

Files: hyperspy/io_plugins/bruker.py
Copyright: 2016 Petras Jokubauskas; 2016 The HyperSpy developers
License: GPL-3

Files: hyperspy/misc/test_utils.py
Copyright: 2011 the scikit-image team; 2007-2016 The HyperSpy developers
License: GPL-3

Files: debian/*
Copyright: 2020 Sebastien Delafond <seb@debian.org>
License: GPL-3

License: GPL-3
 See `/usr/share/common-licenses/GPL-3'.

License: BSD-2
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

License: BSD-3
  Copyright (c) The Regents of the University of California.
  All rights reserved.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
